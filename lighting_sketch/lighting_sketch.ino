#include <FastLED.h>

#define LED_PIN 7
#define NUM_LEDS 60

CRGB leds[NUM_LEDS];

void setup() {
  // put your setup code here, to run once:
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
}

void loop() {
  quickCycle();
  //breathEffect();
}


void breathEffect() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB ( 255, 0, 0);
    FastLED.show();
  }
  
  for (int i = 0; i < 100; i++) {
    FastLED.setBrightness(i);
    delay(500);
  }
}

/**
 * Simple method which cycles through colours
 */
void quickCycle() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB ( 255, 255, 255);
    FastLED.show();
    delay(10);
  }
  
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB ( 255, 0, 0);
    FastLED.show();
    delay(10);
  }

  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB ( 255, 255, 255);
    FastLED.show();
    delay(10);
  }

  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB ( 0, 255, 0);
    FastLED.show();
    delay(10);
  }

  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB ( 255, 255, 255);
    FastLED.show();
    delay(10);
  }

  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB ( 0, 0, 255);
    FastLED.show();
    delay(10);
  }
}
